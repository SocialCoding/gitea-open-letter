from datetime import datetime
from dateutil.parser import parse as parse_date


def write_file(title, contents):
    title = title.strip()
    filename = title.replace(' ', '-').lower()
    if "Current status" in title:
        date = datetime.now()
    else:
        date = parse_date(title)
    filename += '.md'
    with open(f"content/updates/{filename}", "w", encoding='utf-8') as f:
        print(f"[*] Writing update {filename}")
        f.write("+++\n")
        f.write(f"title = '{title}'\n")
        f.write(f"date = '{date}'\n")
        f.write("+++\n")
        f.write(update)


with open("./status.md", "r", encoding='utf-8') as f:
    title = None
    update = ""
    for line in f.readlines():
        if line.startswith("###"):
            if title:
                write_file(title, update)

            title = line.removeprefix("### ")
            update = ""
        else:
            if title:
                if line.startswith("---"):
                    break
                else:
                    update += line

    write_file(title, update)
