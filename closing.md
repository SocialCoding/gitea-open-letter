# Forgejo - Beyond coding. We forge.

After many days of hard work and preparation by a team of former Gitea maintainers and enthusiasts from the FOSS community, we are proud to announce that the Forgejo project is now live.

Forgejo (/forˈd͡ʒe.jo/ inspired by forĝejo – the Esperanto word for forge) is a community-driven Free Software project that develops a code forge platform similar to GitHub, and that is a drop-in replacement for Gitea. We started Forgejo in reaction to control of Gitea being taken away from the community by the newly-formed for-profit company Gitea Ltd without prior community consultation, and after an [Open Letter](https://gitea-open-letter.coding.social/) to the Gitea project owners remained unanswered.
The Forgejo project has two major objectives that drive our development and road map:

1. The community is in control, and ensures we develop to address community needs.
2. We will help liberate software development from the shackles of proprietary tools.

[Read more](https://forgejo.org/2022-12-15-hello-forgejo/).

# Gitea Ltd confirms its takeover of the Gitea project

On 28 October, members of the Gitea Community published the [Gitea Open Letter](https://gitea-open-letter.coding.social/) demanding restitution of the Gitea project after [the takeover announced on 25 October](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/).

After discussions via diplomatic channels about the Gitea Open Letter, Gitea Ltd decided to ignore the demands [and further confirmed they now are in total control of the Gitea project](https://blog.gitea.io/2022/10/a-message-from-lunny-on-gitea-ltd.-and-the-gitea-project/) in a blog post signed by two shareholders, Lunny and techknowlogick on 30 October.

This unfortunately concludes the Gitea Open Letter has failed and there is **no alternative but forking the project under a new name**, with a healthy democratic governance. Exactly as it was before 25 October in the Gitea project. But this time in the context of an incorporated non-profit that provides a legal framework.
