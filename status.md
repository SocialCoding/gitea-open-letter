This page is no longer updated, anyone is welcome to follow https://floss.social/@forgejo instead.

---

### December 15th

After many days of hard work and preparation by a team of former Gitea maintainers and enthusiasts from the FOSS community, we are proud to announce that the Forgejo project is now live.

Forgejo (/forˈd͡ʒe.jo/ inspired by forĝejo – the Esperanto word for forge) is a community-driven Free Software project that develops a code forge platform similar to GitHub, and that is a drop-in replacement for Gitea. We started Forgejo in reaction to control of Gitea being taken away from the community by the newly-formed for-profit company Gitea Ltd without prior community consultation, and after an [Open Letter](https://gitea-open-letter.coding.social/) to the Gitea project owners remained unanswered.
The Forgejo project has two major objectives that drive our development and road map:

1. The community is in control, and ensures we develop to address community needs.
2. We will help liberate software development from the shackles of proprietary tools.

[Read more](https://forgejo.org/2022-12-15-hello-forgejo/).

### December 6th

Forgejo launch is planned for December 15th and this is the last daily update. You are kindly invited to follow the project at [https://floss.social/@forgejo](https://floss.social/@forgejo) from now on, or to join us in our [Matrix chat room](https://matrix.to/#/#forgejo-chat:matrix.org). We welcome suggestions on [our issue tracker](https://codeberg.org/forgejo/forgejo).

### December 5th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### December 4th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### December 3rd

The Forgejo repository and feature branches were rebased on top of the latest Gitea development branch and the v1.18 stable branch. This rebasing happens weekly because Forgejo is a soft fork of Gitea. All CI pipelines are green.

### December 2nd

The 50,000€ [NLnet](https://nlnet.nl/entrust/) grant to further forge federation started December 1st and its progress can be followed in [the Forgejo feature branches](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md#federation-https-codeberg-org-forgejo-forgejo-issues-labels-79349) dedicated to them.

### December 1st

A 50,000€ grant application was sent to [NLnet](https://nlnet.nl/entrust/) to support the development of federated UI/UX.

Hosting for the website https://forgejo.org is ready.

### November 30th

Forgejo has [a logo](https://codeberg.org/forgejo/meta/src/branch/readme/logo/forgejo.svg) and [a mascotte](https://codeberg.org/forgejo/meta/issues/54).

### November 29th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 28th

The Forgejo launch is scheduled for December 8th, 2022. Updates will then be posted on the Forgejo social account at https://floss.social/@forgejo.

### November 27th

The first draft of the [contributor guide](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING.md) is complete, covering all aspects of the Forgejo project: governance, development workflow, release process, security vulnerability handling, Developer Certificate of Origin and funding.

A Forgejo mascot was sketched and discussed: it could be a steampunk squirrel, a mamal that spends a lot of time caring for its food storage.

### November 26th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 25th

A 50,000€ grant application was sent to [NLnet](https://nlnet.nl/entrust/) to support the development of Forgejo. The [packages](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18.0-rc1) and [binaries](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.0-rc1) for the Forgejo 1.18.0-rc1 experimental release are published. The first meeting to define the Forgejo governance happened and another will follow in two weeks.

### November 24th

An [interim Forgejo logo](https://floss.social/@forgejo) is now in use while the definitive logo is designed.

### November 23rd

A social account was created for Forgejo at https://floss.social/@forgejo. It will not publish updates immediately but everyone is welcome to follow.

### November 22nd

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 21st

The link to the [Matrix account](https://matrix.to/#/#forgejo-chat:matrix.org) and [Mastodon profile](https://floss.social/@forgejo) are part of our README in order to get verified links.
Our CI process for updating the website was hardened to limit the blast radius in case of a security incidence in the future.
We are [looking for a tagline](https://codeberg.org/forgejo/meta/issues/42). Have an idea? Let us know!

### November 20th

The proofs were uploaded to keyoxide for [security@forgejo.org](https://keyoxide.org/security@forgejo.org).

### November 19th

The GPG signing keys for the release and security teams have been generated and published. The proofs were uploaded to keyoxide for [contact@forgejo.org](https://keyoxide.org/contact@forgejo.org) and [release@forgejo.org](https://keyoxide.org/release@forgejo.org).

### November 18th

The release process is implemented and documented to publish Forgejo 1.18.0-rc1 as soon as Gitea 1.18.0-rc1 is released.

### November 17th

Preparations are made to publish Forgejo 1.18.0-rc1 as soon as Gitea 1.18.0-rc1 is released. A security team is discussed to cover both Forgejo and Codeberg. The Forgejo domain name (forgejo.org) is in the name of Codeberg e.V. at the registrar. A grant application is drafted to support Forgejo. 

### November 16th

[Forgejo](https://codeberg.org/forgejo/meta/issues/1#issuecomment-688644) is the name that was picked. Codeberg e.V. [explicitly accepted the proposal](https://codeberg.org/forgejo/meta/issues/3#issuecomment-688648) to host and use Forgejo.

The next step is to figure out [how to properly communicate about Forgejo](https://codeberg.org/forgejo/meta/issues/8).

### November 15th

The [development workflow](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/WORKFLOW.md) discussed [over the past week](https://codeberg.org/forgejo/forgejo/issues/5) was [bootstrapped](https://codeberg.org/forgejo/forgejo/issues/12).

### November 14th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 13th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 12th

Discussions [started today to apply for a 50,000€ grant](https://codeberg.org/forgejo/meta/issues/32) to further the project. It is still unclear if all the conditions are met for it to succeed.

### November 11th

There is no high level update today although progress happened.

See [here](https://codeberg.org/forgejo/meta/milestone/2788), [here](https://codeberg.org/forgejo/forgejo/milestone/2794) and [there](https://codeberg.org/forgejo/website/milestone/2793) for the details.

### November 10th

A [Code of Conduct was adopted](https://codeberg.org/forgejo/code-of-conduct) and immediately applies to all spaces of the fork (chatroom, repositories, etc.). A "well being" and a moderation team were appointed and are ready to help preserve an inclusive environment for everyone.

### November 9th

People willing to help launch the fork can [pick a task](https://codeberg.org/forgejo/meta/milestone/2788) and get involved, follow the progress in the [dedicated chatroom](https://matrix.to/#/#codename-updates:matrix.org) or discuss in the space where over sixty people [got together in the past week](https://matrix.to/#/#gitea-fork-on-codeberg:matrix.org).

### November 8th

The launch date for the fork is set to December 15th, but it could happen earlier if all is ready. The [list of tasks](https://codeberg.org/forgejo/meta/milestone/2788) that need to be completed before the launch is finalized and work is in progress.

### November 7th

The process of picking a name for the fork has started and will be completed November 13th. A number of other tasks are being worked on for the launch to happen and can be found in the [corresponding milestone](https://codeberg.org/forgejo/meta/milestone/2788).

These updates will stop when the fork launches and will be replaced by blog posts and release notes.

### November 6th

Early feedback from Codeberg e.V. indicates the proposal to host and use the fork was received positively and a decision can be expected in the following days.

A [temporary organization](https://codeberg.org/codename) was created at Codeberg to prepare for the launch of the soft fork. It has repositories for [non-technical tasks](https://codeberg.org/forgejo/meta), a temporary [repository for the fork](https://codeberg.org/forgejo/codename) and another for the content of the [website](https://codeberg.org/forgejo/website).

### November 5th

The process to pick a name has been established and will now be implemented.

Discussions regarding the long term sustainability covered donations campaigns, grant applications and personnel delegation.

The [Woodpecker CI](https://codeberg.org/Codeberg/Community/issues/782) was setup for the current [Codeberg codebase](https://codeberg.org/Codeberg/gitea/src/branch/wip-codeberg-1.18). If Codeberg agrees to host and use the fork, a similar configuration will be used for the CI.

### November 4th

Around fifty names were suggested (Smithy, GitLibre, LibreForge etc.).

Preliminary technical work started to [use Woodpecker](https://codeberg.org/Codeberg/gitea/pulls/45) for the CI of the fork, instead of Drone which is [proprietary software](https://woodpecker-ci.org/faq#why-is-woodpecker-a-fork-of-drone-version-08).

### November 3rd

An [action plan was proposed to Codeberg e.V.](https://pad.gusted.xyz/pU-UuidJRgmWKrAWawK8mQ?view#) to host and use a Gitea soft fork.

When the fork happens, it should be communicated to the general public in a positive way. Work has been done to create communication material that conveys a broad vision of Free Software development, which will hopefully help convey the cooperative state of mind in which the fork is being prepared.

### November 2nd

The action plans converged into one that is in its final draft stage. A few names were suggested for the fork and registered as a precaution.

### November 1st

More discussions and drafting of two action plans, with a focus on allowing Gitea developers to find a transparent, democratic and Free Software environment.

### October 31th

A plan for a hard fork drafted on October 29th is set aside. A new, low profile action plan based on a soft fork is in draft stage. It focuses on allowing Gitea developers to find a transparent, democratic and Free Software environment and continue their work in an existing organization like Codeberg. It could keep the Gitea community united.

Discussions regarding the claims that Gitea Ltd was created to solve a sustainability problem led to the publication of a blog post: [The Gitea Ltd Sustainability Smokescreen](https://blog.dachary.org/2022/10/31/the-gitea-ltd-sustainability-smokescreen/).

### October 30th

Ambassador(s) are engaged in discussions with the shareholders of the Gitea Ltd company and the members of the Gitea Community who signed the Gitea Open Letter. In the afternoon, a [second blog post is published](https://blog.gitea.io/2022/10/a-message-from-lunny-on-gitea-ltd.-and-the-gitea-project/) and signed by Lunny and techknowlogick.

The shareholders of Gitea Ltd do not agree to the demands of the Gitea Open Letter.

### October 29th

A second blog post is being prepared from the same authors as the original Gitea post.

### October 28th

The announcement of *Gitea Ltd* raised concerns from members of the Gitea community, who [published the Gitea Open Letter](https://gitea-open-letter.coding.social/). It was [advertised](https://codeberg.org/SocialCoding/gitea-open-letter/issues/19) on the fediverse, HN etc. 

### October 25th

A [blog post was written on the Gitea blog](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/), informing about the creation of a company named *Gitea Ltd*.

---

All the discussions and documents archived: if you have any question feel free to ask [in the chatroom](https://matrix.to/#/#gitea-fork-on-codeberg:matrix.org) or read the raw material (it is a lot but it is available publicly, archives are [here](https://matrix.to/#/#gitea-open-letter:matrix.org) and [here](https://matrix.to/#/!SakSkZqjzMsaPCVqlv:matrix.batsense.net/$mQlw3xHwmXAjV0Vs5h2WNBIlcjNRG9L5v2R_uv7B5S4?via=matrix.org&via=t2bot.io&via=envs.net)).
